export class Blogs{
    title: string;
    date: Date;
    name: string;
    email: string;
    message: string;
    id:number;
    comments: {name:string; comment: string; date: Date}[];

    private static idCounter = 1;

    constructor(title:string, name: string, email: string, message:string, id:number) {
        this.title = title;
        this.date = new Date()
        this.name = name;
        this.email = email;
        this.message = message;
        this.id = Blogs.idCounter++;
        this.comments = [];
    }

}