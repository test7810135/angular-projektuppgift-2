import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Blogs } from '../models/blog.model';
import { CommonModule } from '@angular/common';
import { FormComponent } from '../form/form.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-blogs',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './blogs.component.html',
  styleUrl: './blogs.component.css'
})
export class BlogsComponent {
  @Input() blogPost: Blogs;
  @Output() blogClick = new EventEmitter<void>();

  onClick() {
    this.blogClick.emit();
  }
}
