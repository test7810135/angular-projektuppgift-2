import { Component } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { BlogsComponent } from '../blogs/blogs.component';
import { CommonModule } from '@angular/common';
import {Blogs} from '../models/blog.model'

@Component({
  selector: 'app-form',
  standalone: true,
  imports: [FormsModule, BlogsComponent, CommonModule],
  templateUrl: './form.component.html',
  styleUrl: './form.component.css'
})
export class FormComponent {
  blogs: Blogs [] = [];

  blogMessage: string = "";
  blogName: string = "";
  blogTitle: string="";
  blogEmail: string="";
  blogId: number= 0;
  commentName: string;
  commentText: string;

  nextId: number= 1;

  selectedBlog: Blogs | null = null;
  
  
  onSubmit(blogForm: NgForm) {
    if (this.blogTitle && this.blogName && this.blogEmail && this.blogMessage) {
      const newBlog = new Blogs(
        this.blogTitle,
        this.blogName,
        this.blogEmail,
        this.blogMessage,
        this.blogId
      );
  
      this.blogs.push(newBlog);
      blogForm.resetForm();
      // rensa input
      this.blogTitle = '';
      this.blogName = '';
      this.blogEmail = '';
      this.blogMessage = '';
    } else {
      alert('Please fill out all fields.');
    }
    console.log(this.blogs)
  }
  onSelectBlog(blog: Blogs) {
    this.selectedBlog = blog;
  }
  onBack(){
    this.selectedBlog = null;
  }
  addComment() {
    if (this.selectedBlog && this.commentName && this.commentText) {
      this.selectedBlog.comments.push({
        name: this.commentName,
        comment: this.commentText,
        date: new Date()
      });
      this.commentName = '';
      this.commentText = '';
    }
  }
}
