import { Routes } from '@angular/router';
import { BlogsComponent } from './blogs/blogs.component';

export const routes: Routes = [
    {
        path: 'blog/:id',
        component: BlogsComponent
    }
];
